import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NgpSlideBlockModule} from '../../projects/ngp-slide-block/src/lib/ngp-slide-block.module';
import {BaseComponent} from './base/base.component';

@NgModule({
  declarations: [
    AppComponent,
    BaseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgpSlideBlockModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
