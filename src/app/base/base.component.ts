import {Component, OnInit, ViewChild} from '@angular/core';
import {enSmooth} from '../../../projects/ngp-slide-block/src/lib/animate/config';
import {ToggleGroupComponent} from '../../../projects/ngp-slide-block/src/lib/toggle-group/toggle-group.component';
import {enFloat} from '../../../projects/ngp-slide-block/src/lib/toggle-block/const';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnInit {

  @ViewChild(ToggleGroupComponent) group: ToggleGroupComponent;

  easing = enSmooth;
  float = enFloat;

  constructor() { }

  ngOnInit() {
  }

  openSecond() {
    this.group.open(1);
  }

  closeSecond() {
    this.group.close(1);
  }

  closeAll() {
    this.group.closeAll();
  }
}
