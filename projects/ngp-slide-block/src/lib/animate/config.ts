export enum enSmooth {
  ease = 'ease',
  easeIn = 'ease-in',
  easeOut = 'ease-out',
  easeInOut = 'ease-in-out',
  linear = 'linear',
}

