import {enSmooth} from './config';

export interface AnimateOptions {
  delayVal ?: string | number;
  durationVal ?: string | number;
  easingVal ?: enSmooth | string;
}

export class AnimateConfig implements AnimateOptions {
  delayVal: string | number = 0;
  durationVal: string | number = '250ms';
  easingVal: enSmooth | string = enSmooth.easeIn;

  constructor(animation: AnimateOptions = {delayVal: 0, durationVal: 250, easingVal: enSmooth.easeIn}) {
    this.delay = animation ? animation.delayVal : null;
    this.duration = animation ? animation.durationVal : null;
    this.easing = animation ? animation.easingVal : null;
  }

  set delay(value) {
    this.delayVal = AnimateConfig.convertTiming(value);
  }

  get delay(): string | number {
    return this.delayVal;
  }

  set duration(value) {
    this.durationVal = AnimateConfig.convertTiming(value);
  }

  get duration(): string | number {
    return this.durationVal;
  }

  set easing(value) {
    this.easingVal = value;
  }

  get easing(): enSmooth | string {
    return this.easingVal;
  }

  get timing() {
      return `${this.durationVal} ${this.delayVal ? this.delayVal : ''} ${this.easingVal}`;
  }

  get isEmpty(): boolean {
    return this.delay === null && this.duration === null && this.easing === null;
  }

  static convertTiming(timing: string | number): string {
    if (typeof timing === 'number') {
      return timing + 'ms';
    }

    const timingRegexp = /^(\d{1,4}(ms|s)|(\d{1,2}(.\d{1,2})?s))\b/;
    if (timingRegexp.test(timing)) {
      return timing;
    }
  }

  assign(val: AnimateConfig | null) {
    if (!val) {
      return;
    }
    if (val.duration) {
      this.duration = val.duration;
    }
    if (val.delay) {
      this.delay = val.delay;
    }
    if (val.easing) {
      this.easing = val.easing;
    }
  }
}
