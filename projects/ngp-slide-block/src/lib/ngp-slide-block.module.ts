import {NgModule} from '@angular/core';
import {ToggleBlockComponent} from './toggle-block/toggle-block.component';
import {TriggerDirective} from './directives/trigger.directive';
import {ContentDirective} from './directives/content.directive';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToggleGroupComponent} from './toggle-group/toggle-group.component';
import {LabelDirective} from './directives/label.directive';
import {ContentHolderDirective} from './directives/content-holder.directive';

@NgModule({
  declarations: [
    ToggleBlockComponent,
    TriggerDirective,
    ContentDirective,
    ToggleGroupComponent,
    LabelDirective,
    ContentHolderDirective
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule
  ],
  exports: [
    ToggleBlockComponent,
    TriggerDirective,
    LabelDirective,
    ContentDirective,
    ToggleGroupComponent
  ]
})
export class NgpSlideBlockModule {
}
