import {Directive, Input} from '@angular/core';
import {enFloat} from '../toggle-block/const';

@Directive({
  selector: '[ngpLabel]'
})
export class LabelDirective {

  @Input() float: enFloat | null = null;

  constructor() { }

}
