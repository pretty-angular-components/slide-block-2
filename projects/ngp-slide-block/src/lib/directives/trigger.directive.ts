import {Directive, EventEmitter, HostListener, Output} from '@angular/core';

@Directive({
  selector: '[ngpTrigger]'
})
export class TriggerDirective {

  constructor() { }

  @Output() clicked: EventEmitter<any> = new EventEmitter();

  @HostListener('click') onMouseLeave() {
    this.clicked.emit(true);
  }

}
