import {AfterViewInit, Directive, ElementRef, EventEmitter, Input, Output, Renderer2} from '@angular/core';
import {animate, AnimationBuilder, AnimationFactory, AnimationPlayer, group, style} from '@angular/animations';
import {enDirection} from '../toggle-block/const';

@Directive({
  selector: '[ngpContentHolder]'
})
export class ContentHolderDirective implements AfterViewInit {

  private elRect: DOMRect | null;
  private player: AnimationPlayer;

  @Input() active = true;
  @Input() triggerRect: DOMRect = null;

  @Output() animStart: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() animFinish: EventEmitter<boolean> = new EventEmitter<boolean>();

  get rect(): DOMRect {
    return this.elRect;
  }

  constructor(public el: ElementRef, private render: Renderer2, public builder: AnimationBuilder) {
  }

  ngAfterViewInit(): void {
    this.elRect = this.el.nativeElement.getBoundingClientRect();
  }

  animate(dir: enDirection, timing: string, instantly?: boolean) {
    if (this.player && this.player.hasStarted()) {
      this.player.pause();
    }

    const animations = [
      animate(instantly ? 0 : timing, style({width: `${dir === enDirection.close ? 0 : this.rect.width}px`}))
    ];

    if (this.triggerRect.height < this.rect.height) {
      animations.push(animate(instantly ? 0 : timing,
        style({height: `${dir === enDirection.close ? this.triggerRect.height : this.rect.height}px`})
      ));
    }

    const animation: AnimationFactory = this.builder.build([group(animations)]);
    this.player = animation.create(this.el.nativeElement);

    this.player.onStart(() => {
      if (dir === enDirection.open) {
        this.show();
      }
      this.animStart.emit(true);
    });
    this.player.onDone(() => {
      if (dir === enDirection.close) {
        this.hide();
      }
      this.animFinish.emit(true);
    });
    this.player.play();
  }

  hide() {
    this.render.setStyle(this.el.nativeElement, 'width', 0);
    this.render.setStyle(this.el.nativeElement, 'hidden', true);

    if (this.triggerRect && this.triggerRect.height < this.rect.height) {
      this.render.setStyle(this.el.nativeElement, 'height', this.triggerRect.height + 'px');
    }
  }

  show() {
    this.render.setStyle(this.el.nativeElement, 'hidden', false);
  }
}
