import {Directive, ElementRef, Input} from '@angular/core';
import {enFloat} from '../toggle-block/const';

@Directive({
  selector: '[ngpContent]'
})
export class ContentDirective {

  @Input() float: enFloat | null = null;

  constructor() { }
}
