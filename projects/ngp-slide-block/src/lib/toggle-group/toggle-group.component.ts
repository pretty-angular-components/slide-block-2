import {AfterViewInit, Component, ContentChildren, EventEmitter, Input, OnInit, Output, QueryList} from '@angular/core';
import {ToggleBlockComponent} from '../toggle-block/toggle-block.component';
import {AnimateConfig} from '../animate/AnimateConfig';

@Component({
  selector: 'ngp-toggle-group',
  templateUrl: './toggle-group.component.html',
  styleUrls: ['./toggle-group.component.scss']
})
export class ToggleGroupComponent implements OnInit, AfterViewInit {

  @ContentChildren(ToggleBlockComponent) items: QueryList<ToggleBlockComponent>;

  private animationConfig = new AnimateConfig(null);
  private openedItems = [];

  @Input() openLimit: number | boolean = 1;
  @Input() set animationDelay(value: number) { this.animationConfig.delay = value; }
  @Input() set animationDuration(value: number) { this.animationConfig.duration = value; }
  @Input() set animationEasing(value: string) { this.animationConfig.easing = value; }

  @Output() animStart: EventEmitter<number> = new EventEmitter<number>();
  @Output() animFinish: EventEmitter<number> = new EventEmitter<number>();

  get opened(): number[] {
    return this.openedItems;
  }

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.items.forEach((item: ToggleBlockComponent, index: number) => {
      item.groupAnimationConfig = this.animationConfig;
      if (item.isOpened) {
        this.openedItems.push(index);
      }
      item.animStart.subscribe(() => {
        this.updateOpened(item, index);
        this.animStart.emit(index);
      });
      item.animFinish.subscribe(() => this.animFinish.emit(index));
    });
  }

  open(index: number, instantly?: boolean) {
    const item = this.items.find((tmpItem, tmpIndex) => index === tmpIndex);
    if (item) {
      item.open(instantly);
    }
  }

  close(index: number, instantly?: boolean) {
    const item = this.items.find((tmpItem, tmpIndex) => index === tmpIndex);
    if (item) {
      item.close(instantly);
    }
  }

  closeAll(instantly?: boolean) {
    this.items.forEach(item => item.close(instantly));
  }

  private updateOpened(currentItem: ToggleBlockComponent, currentIndex: number) {
    if (currentItem.isOpened) {
      this.openedItems.push(currentIndex);
    } else {
      const index = this.openedItems.indexOf(currentIndex);
      if (index !== -1) {
        this.openedItems.splice(index);
      }
    }
    if (this.openLimit && this.openedItems.length > this.openLimit) {
      const  index = this.openedItems.shift();
      this.items.toArray()[index].close();
    }
  }
}
