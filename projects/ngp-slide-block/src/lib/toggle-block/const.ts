export enum enFloat {
  left = 'left',
  right = 'right',
}

export enum enDirection {
  open,
  close
}
