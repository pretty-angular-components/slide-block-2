import {AfterViewInit, Component, ContentChild, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ContentDirective} from '../directives/content.directive';
import {TriggerDirective} from '../directives/trigger.directive';
import {AnimateConfig} from '../animate/AnimateConfig';
import {enDirection, enFloat} from './const';
import {enSmooth} from '../animate/config';
import {LabelDirective} from '../directives/label.directive';
import {ContentHolderDirective} from '../directives/content-holder.directive';
import { zip } from 'rxjs';

@Component({
  selector: 'ngp-toggle-block',
  templateUrl: './toggle-block.component.html',
  styleUrls: ['./toggle-block.component.scss']
})
export class ToggleBlockComponent implements OnInit, AfterViewInit {

  @ContentChild(TriggerDirective) trigger: TriggerDirective;
  @ContentChild(ContentDirective) content: ContentDirective;
  @ContentChild(LabelDirective) label: LabelDirective;

  @ViewChild('triggerContainer') triggerContainer: ElementRef;
  @ViewChild('contentContainer', { read: ContentHolderDirective }) contentContainer: ContentHolderDirective;
  @ViewChild('labelContainer', { read: ContentHolderDirective }) labelContainer: ContentHolderDirective;

  private animationGroupConfig: AnimateConfig | null = null;
  private animationConfig = new AnimateConfig(null);

  @Output() animStart: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() animFinish: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input() isOpen = false;
  @Input() float: enFloat | string = enFloat.right;
  @Input() set animationDelay(value: number | string) { this.animationConfig.delay = value; }
  @Input() set animationDuration(value: number | string) { this.animationConfig.duration = value; }
  @Input() set animationEasing(value: string | enSmooth) { this.animationConfig.easing = value; }

  set groupAnimationConfig(config: AnimateConfig) {
    this.animationGroupConfig = config;
  }

  get isOpened(): boolean {
    return this.isOpen;
  }

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    if (!this.content) {
      throw new Error(`can't find content, at least one html tag must contain ngpContent directive`);
    }

    const triggerRect = this.triggerContainer ? this.triggerContainer.nativeElement.getBoundingClientRect() : null;
    this.labelContainer.triggerRect = triggerRect;
    this.contentContainer.triggerRect = triggerRect;

    zip(this.labelContainer.animStart, this.contentContainer.animStart).subscribe(() => this.animStart.emit(true));
    zip(this.labelContainer.animFinish, this.contentContainer.animFinish).subscribe(() => this.animFinish.emit(true));

    if (this.isOpened) {
      this.labelContainer.hide();
    } else {
      this.contentContainer.hide();
    }

    if (this.trigger) {
      this.trigger.clicked.subscribe(() => {
        this.isOpen = !this.isOpen;
        this.animate();
      });
    }
  }

  open(instantly?: boolean) {
    if (!this.isOpened) {
      this.isOpen = true;
      this.animate(instantly);
    }
  }

  close(instantly?: boolean) {
    if (this.isOpened) {
      this.isOpen = false;
      this.animate(instantly);
    }
  }

  private animate(instantly?: boolean) {
    this.labelContainer.animate(this.isOpen ? enDirection.close : enDirection.open, this.animTiming(), instantly);
    this.contentContainer.animate(this.isOpen ? enDirection.open : enDirection.close, this.animTiming(), instantly);
  }

  private animTiming() {
    const animConf = new AnimateConfig();
    animConf.assign(this.animationGroupConfig);
    animConf.assign(this.animationConfig);

    return animConf.timing;
  }

  contentFloat(): string {
    return this.content && this.content.float ? this.content.float : this.float;
  }

  labelFloat(): string {
    return this.label && this.label.float ? this.label.float : this.float;
  }

}
