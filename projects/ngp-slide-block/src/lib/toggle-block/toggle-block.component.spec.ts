import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToggleBlockComponent } from './toggle-block.component';

describe('ToggleBlockComponent', () => {
  let component: ToggleBlockComponent;
  let fixture: ComponentFixture<ToggleBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToggleBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
