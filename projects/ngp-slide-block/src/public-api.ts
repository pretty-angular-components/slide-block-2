/*
 * Public API Surface of ngp-slide-block
 */

export * from './lib/animate/config';
export * from './lib/toggle-block/const';
export * from './lib/toggle-block/toggle-block.component';
export * from './lib/toggle-group/toggle-group.component';
export * from './lib/directives/trigger.directive';
export * from './lib/directives/label.directive';
export * from './lib/directives/content.directive';
export * from './lib/ngp-slide-block.module';

