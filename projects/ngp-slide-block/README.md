# NgpSlideBlock

This is simple slider-switch for opening and closing *HTML* elements such as inputs, textarea, links. 

|Library version|npm tag|Angular version|
|---|:---:|:---:|
|1.x|ng8|Angular 8|
|2.x|ng9|Angular 9|

Site with demos available here: [prettyangular.kinect.pro](https://prettyangular.kinect.pro)

**Note:** If you develop for IE / Edge browser, do not forget include all required polyfills, 
such as `classlist.js` and `web-animations-js`

## Installation

### How to install

##### With npm
```npm install @kinect-pro/ngp-slide-block```
##### With yarn
```yarn add @kinect-pro/ngp-slide-block```

### How to setup

#### Add NgpSlideBlockModule to your App module 

##### app.module
```
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgpSlideBlockModule } from '@kinect-pro/ngp-slider-block';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    BaseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgpSlideBlockModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

#### Simplest example: using one toggling item  
##### app.component.html
**Example 1:**
```
<ngp-toggle-block [isOpen]="true">
  <button class="btn btn-primary" ngpTrigger>switch</button>
  <input class="form-control" type="text" value="test" name="text" ngpContent>
</ngp-toggle-block>
```

**Example 2:**
```
<ngp-toggle-block>
  <button class="btn btn-primary" ngpTrigger>Show</button>
  <label class="p-1" ngpLabel  [float]="float.left">Hi, this is some information for user</label>
  <input class="form-control" type="text" value="test" name="text" ngpContent>
</ngp-toggle-block>
```

* ngpTrigger - directive for mark the object  clicking on which will hide or show content
* ngpLabel - label block for hiding, non required element
* ngpContent - content block for hiding

## Properties
<table>
    <tr>
        <th>Property</th>
        <th>Type</th>
        <th>Default</th>        
        <th>Description</th>
    </tr>    
    <tr>
        <td>isOpen</td><td>boolean</td><td>false</td>
        <td>Initial state of slide, show or hide content by default.</td>        
    </tr>
    <tr>
        <td>float</td><td>enFloat | string</td><td>right</td>
        <td>
        Setts alignment of content, enum variable can be 'right' or 'left'.
        Besides this property exists in the ngpLabel and ngpContent. 
        </td>                
    </tr>
    <tr>
        <td>animationDelay</td><td>number</td><td>0</td><td></td>
    </tr>
    <tr>
        <td>animationDuration</td><td>number</td><td>250</td><td></td>
    </tr>
    <tr>
        <td>animationEasing</td><td>enSmooth | string</td><td>'ease-in'</td><td></td>
    </tr>
</table>

## Methods

You can manipulate slider manually by using `@ViewChild()`  
At this moment available methods:  
**open(instantly?: boolean)** - open slider     
**close(instantly?: boolean)** - close slider    

## Events

You can subscribe on two event emitters:   
**animStart**:  - emit boolean value when animation is started  
**animFinish**: - emit boolean value when animation is finished  

## ngpToggleGroup
For grouping separate slides you can use ngpToggleGroup

```
<ngp-toggle-group [animationDuration]="200" [animationEasing]="easing.easeOut" [openLimit]="2">
  <ngp-toggle-block [isOpen]="true">
    <button class="btn btn-primary" ngpTrigger>1</button>
    <input class="form-control" type="text" value="test" name="text1" ngpContent>
  </ngp-toggle-block>

  <ngp-toggle-block [animationDuration]="1000">
    <button class="btn btn-primary" ngpTrigger>2</button>
    <input class="form-control" type="text" value="test" name="text2" ngpContent>
  </ngp-toggle-block>

  <ngp-toggle-block>
    <a class="btn btn-link" ngpTrigger>link</a>
    <input class="form-control" type="text" value="link test" name="text2" ngpContent>
  </ngp-toggle-block>

  <ngp-toggle-block>
    <img height="42" width="42" class="img-fluid" src="..." ngpTrigger>
    <input class="form-control" type="text" value="link test" name="text2" ngpContent>
  </ngp-toggle-block>
</ngp-toggle-group>
```

## Input properties
<table>
    <tr>
        <th>Property</th>
        <th>Type</th>
        <th>Default</th>        
        <th>Description</th>
    </tr>    
    <tr>
        <td>openLimit</td><td>number | boolean</td><td>1</td>
        <td>By default only one item can be opened at same time. 
        This field allow to change or turn off limitation 
        </td>
    </tr>
    <tr>
        <td>animationDelay</td><td>number</td><td>0</td>
        <td>Default animation delay for all items</td>
    </tr>
    <tr>
        <td>animationDuration</td><td>number</td><td>250</td>
        <td>Default animation duration for all items</td>
    </tr>
    <tr>
        <td>animationEasing</td><td>enSmooth | string</td>
        <td>'ease-in'</td>
        <td>Default animation style for all items</td>
    </tr>
</table>

## Methods

You can manipulate slider manually by using `@ViewChild()`  
At this moment available methods:
**opened()** - return array of indexes of opened items  
**open(index: number, instantly?: boolean)** - open one slider     
**close(index: number, instantly?: boolean)** - close one slider
**closeAll(instantly?: boolean)** - close all opened sliders    

## Events

You can subscribe on two event emitters:   
**animStart**:  - emit index (number) of item which animation is started  
**animFinish**: - emit index (number) of item which animation is finished  
